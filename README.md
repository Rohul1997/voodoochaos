VoodooChaos is a tool that I wrote that will allow the user to open a CIA file and display the title version and then you can edit the title version and save it.

You can open up an NCCH.header file, which will display the Unique ID and the Title ID. You can then edit these information and save it.

You can also open up an Exheader.bin file, which will display the Unique ID and then you can edit and save it.